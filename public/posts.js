window.addEventListener('load', enablePosts);

function enablePosts() {
  let $index = 0; // global variable to hold current post index
  let $posts; // global variable to hold all the posts

  document.getElementById('getPosts').addEventListener('click', getPosts);
  document.getElementById('prevPost').addEventListener('click', showPrevPost);
  document.getElementById('nextPost').addEventListener('click', showNextPost);

  function getPosts() {
    fetch('https://jsonplaceholder.typicode.com/posts')
      .then(res => res.json())
      .then((data) => {
        data.shift();
        $posts = data;
        displayPost($index);
      }); // 'posts' should hold the array of posts on btn click.
    //  As the first element (data[0]) contains meta info which does not match the 'post' object,
    // we remove it by calling 'Array.prototype.shift()' before assigning to 'posts'.
    //  We also display the first post in the '#output' element.
  }

  function displayPost(index) {
    document.getElementById('output').innerHTML = output($posts[currentIndex($index)]);
  }

  function output(post) {
    return `<div class="card card-body mb-3">
      <h3>${post.title}</h3>
      <p>${post.body}</p>
    </div>`;
  }

  function currentIndex(index) {
    const len = $posts.length;
    return ((len + index) % len) % len;
  }

  function showPrevPost() {
    if ($index > 0) {
      $index -= 1;
      displayPost($index);
    }
  }

  function showNextPost() {
    if ($index < ($posts.length - 1)) {
      $index += 1;
      displayPost($index);
    }
  }
}
